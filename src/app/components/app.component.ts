import { Component } from '@angular/core';
import { Contact } from '../model/contact';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public contact:Contact = new Contact ("","","")
  public contacts:Contact[] = [];
  public activeContact:Contact|null = null;

  addContact(){
    this.contacts.push(this.contact.clone());
  }
  choseContact(contact:Contact){
    this.activeContact = contact;
  }
  deleteContact(contact:Contact){
    this.contacts = this.contacts.filter(c=>c!==contact)
    if(this.activeContact==contact) this.activeContact=null;
  }
}
